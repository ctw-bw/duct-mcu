#include <Arduino.h>
#include "duct.h"

const int Duct::header_size = 4;
const byte Duct::send_header[4] = {0x5E, 0xB5, 0xC8, 0xB9};

// construct
Duct::Duct() {

    // Initialize arrays
    for (uint8_t code_i = 0; code_i < STRUCTS_MAX; code_i++) {
        structs_[code_i] = NULL;
        struct_sizes_[code_i] = 0;
    }

    set_state(HEADER);
}

// register_struct
void Duct::register_struct(byte* struct_addr, uint16_t struct_size, uint8_t code) {

    if (code >= STRUCTS_MAX) {
        return; // Invalid code, do nothing
    }

    bool is_new = !valid_code(code);

    structs_[code] = struct_addr;
    struct_sizes_[code] = struct_size;

    if (is_new) {
        num_structs_++; // Increase count
    }
}

// valid_code
bool Duct::valid_code(uint8_t code) {

    if (code >= STRUCTS_MAX) {
        return false; // Definitely not valid
    }

    return structs_[code] != NULL; // If NULL, this code is not yet used
}

// send()
bool Duct::send(uint8_t code, uint16_t offset, uint16_t num_bytes) {

    //Serial.println("Sending...");

    if (!Serial.availableForWrite()) {
        // If the write buffer is full, just return. If we try to write,
        // it becomes blocking, which could be problematic.
        return false;
    }

    //Serial.println("Send 1");

    if (!valid_code(code)) {
        return false; // This code is unknown
    }

    if (num_bytes == 0) {
        num_bytes = struct_sizes_[code] - offset;
    }

    //Serial.println("Send 2");

    if (offset + num_bytes > struct_sizes_[code]) {
        return false; // Invalid, these are more bytes than available in the  struct
    }

    //Serial.println("Send 3");

    Serial.write(send_header, header_size);
    Serial.write(code);
    Serial.write((byte*)&offset, 2);
    Serial.write((byte*)&num_bytes, 2);
    Serial.write(structs_[code] + offset, num_bytes);

    //Serial.println("Send 4");

    return true;
}

// send_addr
bool Duct::send_by_addr(byte* struct_addr, uint16_t offset, uint16_t num_bytes) {
    
    uint8_t code;
    bool found = false;
    for (uint8_t i = 0; i < STRUCTS_MAX; i++) {
        if (structs_[i] == struct_addr) {
            code = i;
            found = true;
            break;
        }
    }
    if (!found) {
        return false; // Struct must not have been registered
    }

    return send(code, offset, num_bytes);
}

// receive
bool Duct::receive() {

    size_t bytes_available = Serial.available();
    if (bytes_available == 0) {
        return false; // No incoming data
    }

    uint8_t buffer[64]; // Serial buffer is max 64 bytes anyway
    size_t bytes_read = Serial.readBytes(buffer, bytes_available);

    if (bytes_read == 0) {
        return false;
    }

    return receive_bytes(buffer, bytes_read);
}

// receive_bytes
bool Duct::receive_bytes(const uint8_t* data, size_t size) {
    
    size_t i = 0;

    while (i < size) {

        Serial.print("State: "); Serial.print(state_);
        Serial.print(", i: "); Serial.print(i);
        Serial.print(", bytes_count: "); Serial.print(bytes_count_);
        Serial.print(", size: "); Serial.println(size);

        if (state_ == HEADER) {
            // Count consecutively correct header bytes
            if (data[i] == send_header[i]) {
                bytes_count_++;
            } else {
                bytes_count_ = 0;
            }

            if (bytes_count_ == header_size) { // Complete header received
                set_state(META); 
            } else if (bytes_count_ > header_size) {
                set_state(HEADER); // Something went wrong, reset
            }

            i++;
            continue;
        }

        if (state_ == META) {
            // Number of bytes to copy is what's needed to finish the meta block
            // or all bytes available, whichever is smaller
            size_t new_bytes = min(5 - bytes_count_, size - i);
            memcpy(&buffer_[bytes_count_], &data[i], new_bytes);
            bytes_count_ += new_bytes; // Added more bytes

            if (bytes_count_ == 5) { // Meta block full
                incoming_code_ = buffer_[0];
                incoming_offset_ = (buffer_[1] << 8) | buffer_[2];
                incoming_size_ = (buffer_[3] << 8) | buffer_[4];

                Serial.print("code: "); Serial.print(incoming_code_);
                Serial.print(", offset: "); Serial.print(incoming_offset_);
                Serial.print(", size: "); Serial.println(incoming_size_);

                incoming_valid_ = valid_code(incoming_code_) && (incoming_size_
                         + incoming_offset_ <= struct_sizes_[incoming_code_]);

                if (incoming_valid_) {
                    set_state(DATA);
                } else {
                    set_state(HEADER); // Something went wrong, better reset
                }
            } else if (bytes_count_ > 5) {
                set_state(HEADER); // Something went wrong, better reset
            }

            i += new_bytes;
            continue;
        }

        if (state_ == DATA) {

            Serial.print("Valid:"); Serial.println(incoming_valid_);

            if (!incoming_valid_) {
                i++;
                continue;
            }

            // TODO: This copy must give an error somewhere, somehow

            // Number of bytes to copy is what's needed to finish the meta block
            // or all bytes available, whichever is smaller
            size_t new_bytes = min(incoming_size_ - bytes_count_, size - i);
            memcpy(&buffer_[bytes_count_], &data[i], new_bytes);
            bytes_count_ += new_bytes; // Added more bytes

            Serial.print("Bytes in data: "); Serial.println(new_bytes);

            if (bytes_count_ == incoming_size_) { // All data received

                // Copy incoming buffer into struct
                memcpy(structs_[incoming_code_] + incoming_offset_, buffer_, incoming_size_);

                Serial.print("Wrote bytes to struct: "); Serial.println(incoming_size_);

                set_state(HEADER);
            } else if (bytes_count_ > incoming_size_) {
                set_state(HEADER); // Something went wrong
            }

            i += new_bytes;
            continue;
        }

    }

}

// set_state
void Duct::set_state(State new_state) {
    state_ = new_state;
    bytes_count_ = 0;

    if (new_state == HEADER) {
        incoming_code_ = 0;
        incoming_offset_ = 0;
        incoming_size_ = 0;
        incoming_valid_ = false; // 'pull down'
    }
}
