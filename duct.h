#ifndef DUCT_H_
#define DUCT_H_

#ifndef STRUCTS_MAX
#define STRUCTS_MAX 16
#endif

// Number of bytes to buffer while recieving data - we avoid writing directly
// to the struct because the data could be broken up, making use write e.g. half
// a float, with bad results
#ifndef DUCT_BUFFER_SIZE
#define DUCT_BUFFER_SIZE 128
#endif

#include <Arduino.h>

/**
 * Struct-based Serial communication - Duct.
 * 
 * Duct works by registering a set of structs on a two devices. Both devices can
 * then send, receive and request the other's bytes in those structs.
 * 
 * This version relies on Arduino's `Serial`.
 * 
 * A fixed size array of pointers to structs is kept. This limits the number
 * of structs to STRUCTS_MAX (16 by default), also affected by the
 * uint8_t type for a struct code.
 * You can pre-define STRUCTS_MAX to at must 256.
 * Using a smaller fixed size reduces the total size of the program.
 */
class Duct {

public:

    /**
     * Different states while reading data
     */
    enum State {HEADER, META, DATA};

    /**
     * @brief Construct a new Duct object.
     */
    Duct();

    /**
     * @brief Register a struct in the Duct instance.
     * 
     * There is no protection about duplicate codes! A previously registered struct
     * will be quietly overwritten.
     * 
     * @param struct_addr 
     * @param struct_size 
     * @param code 
     */
    void register_struct(byte* struct_addr, uint16_t struct_size, uint8_t code);

    /**
     * @brief Return true of code was registered.
     * @param code 
     */
    bool valid_code(uint8_t code);

    /**
     * @brief Send bytes (non-blocking).
     * 
     * If the write buffer is full, the method will abort without sending
     * anything.
     * 
     * @param code      Code of the struct
     * @param offset    Offset inside the struct (default: entire struct)
     * @param num_bytes Number of bytes to send (default: entire struct)
     * @return bool     Return false if nothing is send
     */
    bool send(uint8_t code, uint16_t offset = 0, uint16_t num_bytes = 0);

    /**
     * @brief Send bytes (non-blocking) (using struct address).
     * 
     * If the write buffer is full, the method will abort without sending
     * anything.
     * 
     * @param struct_addr   Address of the first byte of the struct
     * @param offset    Offset inside the struct (default: entire struct)
     * @param num_bytes Number of bytes to send (default: entire struct)
     * @return bool     Return false if nothing is send
     */
    bool send_by_addr(byte* struct_addr, uint16_t offset = 0, uint16_t num_bytes = 0);

    /**
     * @brief Check for any incomding bytes.
     * 
     * Incoming bytes are processed if there are any. This method will return 
     * immediately if there are no bytes available, but will hang untill processing 
     * is finished.
     * 
     * @return     Return true if some new data was read and processed
     */
    bool receive();

    static const int header_size;
    static const byte send_header[4];

private:

    /**
     * @brief Process incoming bytes.
     * 
     * @param data 
     * @param size 
     * @return          True if suitable data was found
     */
    bool receive_bytes(const uint8_t* data, size_t size);

    /**
     * @brief Set the state object
     * 
     * @param new_state 
     */
    void set_state(State new_state);

    uint8_t num_structs_; ///< Number of registered structs

    // The indices of these arrays is their struct code
    byte* structs_[STRUCTS_MAX]; ///< Array of pointers to the first byte of the structs
    uint16_t struct_sizes_[STRUCTS_MAX]; ///< Array of struct sizes

    State state_; ///< Current receive state
    uint8_t buffer_[DUCT_BUFFER_SIZE]; ///< Incoming data buffer
    size_t bytes_count_; ///< Number of received bytes

    /** Last received meta info */
    uint8_t incoming_code_;
    uint16_t incoming_offset_;
    uint16_t incoming_size_;
    bool incoming_valid_; ///< Must be set to 'true' to confirm each incoming packet
};

#endif // DUCT_H_
