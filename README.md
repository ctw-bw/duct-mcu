# Duct for Arduino

Duct is a package to communicate between two devices over a serial connection based on mutually registered structs. (The name is a play on 'struct' and a 'duct' being a type of channel.)

## Arduino Serial

This version relies on Arduino's `Serial`. Note that `Serial.write()` is not blocking, for as long as the write buffer is not full.

## Example

A program to send over a simply struct in it's entirely might look like:

```c++

struct MyData {
    
    float data1;
    float data2;
    float data3;
    uint32_t counter;
};

MyData data;

Duct duct;

void setup() {
    Serial.begin(115200);

    data.data1 = 3.14f;
    data.data2 = 420.0f;
    data.data3 = 1651.16541f;
    data.counter = 0;

    duct.register_struct((byte*)&data, sizeof(data), 0x00);
}

void loop() {

    data.counter++;

    duct.send(0x00, 12, 4); // Send over only the counter field

    duct.receive(); // Process incoming data
}
```

## Struct Padding

The basics of Duct is structs. The danger of structs in struct padding: a C/C++ compiler tends to rearrange a struct differently from expected. Take the following example:

```c++
struct MyStruct {
    uint16_t id1;
    uint16_t id2;
    uint8_t mark;
    uint16_t id3;
    float percentage;
};
```

You would expect a struct instance is: 2 + 2 + 1 + 4 = 9 bytes. But running `cout << sizeof(my_struct_instance);` shows it's actually 12 bytes. It turns out that C++ tries to rearrange the bytes to match the largest property of the struct. In this example that is 4 bytes. So this struct is stored in memory as:

```
x   x   x   x   |   x   0   0   0   |   x   x   x   x
id1     id 2        mark   [padding]    percentage
```

The compiler added three bytes for padding, to make the struct fit blocks of 4 bytes.

There are two solutions:

 * Define your struct such that no padding is needed (by grouping smaller properties together to form blocks of equal size to the biggest property). Test this with `sizeof()`.
 * Add a compiler flag to disable padding (like `#pragma pack(push, 1)`)
    * Python's `ctypes.Structure` also accepts the `_pack_ = 1` property.

*Note: Arduino actually does not add struct padding by default!*
